//LED

#include "LED_pattern.h"
#include "main.h"
#include "macro.h"


//Action, PWM percent, time in 10ms
std::vector<LED_Action> rise_fall {
{ Action::Hold, 10, 20 },
{ Action::Rising, 60, 200 },
{ Action::Hold, 60, 20 },
{ Action::Rising, 10, 200 },
};

std::vector<LED_Action> on_with_pulse {
{ Action::Hold, 60, 280 },
{ Action::Hold, 100, 30 },
};

std::vector<LED_Action> on_with_two_pulse {
{ Action::Hold, 70, 280 },
{ Action::Hold, 100, 20 },
{ Action::Hold, 70, 5 },
{ Action::Hold, 85, 20 },
};

std::vector<LED_Action> solid_50 {
{ Action::Hold, 50, 10 },
};

std::vector<LED_Action> solid_40 {
	{ Action::Hold, 40, 10 },
};

std::vector<LED_Action> solid_20 {
	{ Action::Hold, 20, 10 },
};

std::vector<LED_Action> simple_flash {
	{ Action::Off, 4, 30 },
	{ Action::Hold, 100, 100 }
};

std::vector<LED_Action> pulse_test {
	{ Action::Hold, 50, 150 },
	{ Action::Hold, 100, 20 },
	{ Action::Falling, 20, 20 },
};

std::vector<LED_Action> daytime {
	{ Action::Hold, 2, 210 },
	{ Action::Hold, 100, 25 },
};

std::vector<LED_Action> off {
	{ Action::Off, 0, 10 },
};


//std::initializer_list<std::vector<LED_Action>> patterns = { rise_fall, on_with_pulse, on_with_two_pulse, solid_50, simple_flash};		//
//std::vector<LED_Action> patterns[] = {on_with_two_pulse, pulse_test, simple_flash, on_with_pulse, solid_50, rise_fall, off};		//for helmet 4V
//std::vector<LED_Action> patterns[] = {off, pulse_test, daytime, on_with_two_pulse, solid_40, rise_fall, solid_20, off};							//for 12V bike
//std::vector<LED_Action> patterns[] = {off, pulse_test };
std::vector<LED_Action> patterns[] = {pulse_test, on_with_two_pulse, rise_fall, off};							//for 8V bike


LED_pattern::LED_pattern(void)
{
	reset();
	current_pattern = &patterns[0];
}

//run current pattern
int32_t LED_pattern::run() 
{
	int32_t mPercent = 0;
	static int32_t last_mPercent = 0;
	static uint32_t pattern_count = 0;
	static uint32_t pwm_step = 0;
	
	
	if (count_main >= (current_pattern->size()))		// if > current pattern length, exception if past bounds
		count_main = 0;
	
	volatile const LED_Action & curr = current_pattern->at(count_main);
	
	mPercent = last_mPercent;	//
	
	if (pattern_count == 0)		//if the first iteration of the step
		{
			pattern_count = curr.time;   	//load how long  (+1 ??
			
			switch(curr.action)
			{
			case Action::Off:
				mPercent = 0;
				break;
			case Action::Hold:
				mPercent = curr.percent * mPercent_Mul;
				break;
			case Action::Rising:		//rise/fall is the same code then?
				pwm_step = ((curr.percent * mPercent_Mul) - last_mPercent) / curr.time; //find step required.
				break;
			case Action::Falling:
				pwm_step = ((curr.percent * mPercent_Mul) - last_mPercent) / curr.time;  		//find step required.
				break;
			}
		}
	else						//else start counting down
		{
			if ((curr.action == Action::Rising) || (curr.action == Action::Falling))
			{
				mPercent = last_mPercent + pwm_step;
			}
			
			pattern_count--;
			
			if (pattern_count == 0)
				count_main++;			//once we reach end of time, move to next pattern step
		}
	
	//std::vector<int>::iterator it = vector.begin();		//iterator
	
	last_mPercent = mPercent;
	return mPercent;
}

//Go to next pattern
void LED_pattern::next_pattern()
{
	static uint32_t x = 0;
	
	if (++x >= ARR_LEN(patterns))
		x = 0;
	
	reset_counters();		//
	run();					//set internal state to off
	
	current_pattern = &patterns[x];	
}

LED_Action LED_pattern::get_pattern()
{
	return *current_pattern->data();
}


//Increment 
void LED_pattern::increment() 
{
	//FFFFFF check for overflow issues?
	
	count_main++;
}

//reset current pattern to start and internal timers
void LED_pattern::reset() 
{
	count_main = 0;	
	count_action = 0;
	current_pattern = &off;
}

void LED_pattern::reset_counters() 
{
	count_main = 0;	
	count_action = 0;
}