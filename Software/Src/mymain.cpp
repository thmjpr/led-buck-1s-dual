/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "gpio.h"
#include "iwdg.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "macro.h"
#include "math.h"
#include "LED_pattern.h"
//#include "ui_button_state.h"
//#include "LED_ring.h"


//TODO:
//add ADC watchdog, or other battery undervolt detection


/* Private define ------------------------------------------------------------*/
enum class buttons
{
	SW0,
	SW1,
};

/* Private variables ---------------------------------------------------------*/
volatile bool update_ui, update_led;
volatile uint16_t buttons_pressed;
LED_pattern led_pattern;
//UI ui(&LED);

/* Private function prototypes -----------------------------------------------*/
extern "C" {int mymain(void); void my_Error_Handler(const char* reason); }

/* USER CODE BEGIN PFP */
uint32_t get_vbus_voltage_mV(void);
uint32_t read_buttons(void);
void led_pwm(int32_t channel, int32_t percent);

void my_Error_Handler(const char* reason);


/* Private user code ---------------------------------------------------------*/

int __attribute__((optimize("O0"))) mymain(void)
{	
	uint32_t timeout = 0;
	int32_t led_brightness;
	
	HAL_IWDG_Refresh(&hiwdg);			//Watchdog refresh
	start_adc();						//Begin scanning adc
	HAL_TIM_Base_Start_IT(&htim14);		//Start the timer running (PWM)
	__HAL_DBGMCU_FREEZE_IWDG();			//Freeze the watchdog when debugging and paused
	HAL_Delay(5);						//Wait for ADC to update
	cal_vref_int();						//Calibrate internal vref
	
	
	if (get_vcc_voltage_mV() < TO_mV(2.50))			//Low Vcc voltage
	{
		while (1) 
		{
			HAL_SuspendTick();
			HAL_PWR_EnterSTANDBYMode();			//Enter deep sleep
		}
	}
	
	if (get_temperature_mC() > TO_mC(75))			//High MCU temperature
	{
		while (1) 
		{
			HAL_SuspendTick();
			HAL_PWR_EnterSTANDBYMode(); //Enter deep sleep
		}
	}	
	
	//LED.reset();									//Start at highish brightness, all LEDs
	HAL_TIM_Base_Start_IT(&htim1);					//Start the timer running (UI + LED)
	
	//---------------------------------
	while (1)
	{
		if (update_ui)
		{
			update_ui = false;	
			
			int32_t temp_mC = get_temperature_mC();
			HAL_IWDG_Refresh(&hiwdg);						//Watchdog refresh
			buttons_pressed = read_buttons();
			//ui.state_machine(buttons_pressed);				//run UI task every 10ms
			
			int32_t batt_mV = get_battery_voltage_mV(BATT_DIV_RATIO);
			
			if (get_temperature_mC() > TO_mC(70.0))
			{
				//LED.off();
				// really bad, shut down? go to some kinda error flash mode?
			}
			
			led_brightness = led_pattern.run();
			led_pwm(1, (int32_t)(led_brightness));		//Front light
			led_pwm(2, (int32_t)(led_brightness));		//Back light	
		}
	}
	return 0;
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

}


//Approx 200 Hz
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef* htim)
{
	static int counter = 0;

	if (htim->Instance == TIM1)
	{}
	if (htim->Instance == TIM14)
	{
		//Run LED PWM task every 5ms
		update_led = true;
		update_ui = true;
	}
}


void get_device_id(void)
{
	uint32_t dev_id, rev_id;

	dev_id = HAL_GetDEVID();
	rev_id = HAL_GetREVID();
}



uint32_t get_vbus_voltage_mV(void)
{
	return 0;
}


uint32_t read_buttons(void)
{
	static uint32_t last_press = 0xFFFFFFFF;
	static uint32_t last_rf_data = 0xFFFFFFFF;
	uint32_t tick = HAL_GetTick();
	uint32_t buttons = 0;

	last_press = tick;
	buttons = ~(GPIOF->IDR) & (Button_Pin);
		
	return buttons;
}

//Adjust PWM to LED PAM2804 enable pin
//TIM1_CHx is main PWM output
//PWM frequency ~500Hz ?

void led_pwm(int32_t channel, int32_t mPercent)
{
	int32_t timer_val;
	int32_t timer_range = __HAL_TIM_GET_AUTORELOAD(&htim1); //gets the Period set for PWM (2000?)
	
	//limit to 0-100.000% brightness
	constrain(&mPercent, 0, mPercent_Max);
	
	//convert percent to time range
	timer_val = (mPercent * timer_range) / mPercent_Max;
	
	if (channel == 1)
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_2, (uint16_t)timer_val);	//set PWM duty cycle
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);							//needed to enable timer
	}
	if (channel == 2)
	{
		__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_3, (uint16_t)timer_val);	//set PWM duty cycle
		HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);							//needed to enable timer
	}
	
}


//------------- Errors

void my_Error_Handler(const char* reason)
{
	asm("bkpt 255");

	while (1)
	{
		//watchdog
	}
}
