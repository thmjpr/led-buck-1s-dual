#pragma once
#include <cstdio>

template<typename T> inline
const T&
max(const T& a, const T& b)
{
	return (a > b) ? a : b;
}


template<typename T> inline
const T&
constrain(const T& value, const T& min, const T& max)
{
	if (value > max)
		return max;
	else if (value < min)
		return min;
	else
		return value;
}


//------ Macros ------
static inline void constrain(float * number, float constraint_low, float constraint_high)
{
	//assert(constraint >= 0);
	if (*number > constraint_high)
		*number = constraint_high;
	else if (*number < constraint_low)
		*number = constraint_low;
	else
		return;
}

static inline void constrain(int32_t * number, int32_t constraint_low, int32_t constraint_high)
{
	//assert(constraint >= 0);
	if (*number > constraint_high)
		*number = constraint_high;
	else if (*number < constraint_low)
		*number = constraint_low;
	else
		return;
}

static inline void constrain(uint32_t * number, uint32_t constraint_low, uint32_t constraint_high)
{
	//assert(constraint >= 0);
	if (*number > constraint_high)
		*number = constraint_high;
	else if (*number < constraint_low)
		*number = constraint_low;
	else
		return;
}


//Limit a signed integer to +/- limit number
//positive number only for constraint
static inline void constrain_pm(int32_t * number, int32_t constraint)
{
	//assert(constraint >= 0);
	if (*number > constraint)
		*number = constraint;
	else if (*number < -constraint)
		*number = -constraint;
	else
		return;
}



static uint32_t abs_diff_32(uint32_t number1, uint32_t number2)
{
	int64_t diff = (uint64_t)number1 - (uint64_t)number2;

	if (diff < 0)
		return (uint32_t)(-diff);
	else
		return (uint32_t)diff;
}


static bool single_bit_set(uint16_t number)
{
	if (number != 0 && (number & (number - 1)) == 0)
		return true;
	else
		return false;
}


#define ARR_LEN(x) (sizeof(x)/sizeof(*x))


#define HAL_LOW(port, idx) do{(port)->BRR = (idx);}while(0)
#define HAL_HI(port, idx) do{(port)->BSRR = (idx);}while(0)

//#define HAL_LOW(port, idx) do{(port)->BSRR = (idx);}while(0)
//#define HAL_HI(port, idx) do{(port)->BSRR = (16+idx);}while(0)

#define HAL_IO_TO_GPO(port, idx) do{ uint32_t _tmp_ = (port)->MODER; _tmp_ &= ~(((idx)*2+1)); _tmp_ |= ((idx)*2); (port)->MODER = _tmp_; }while(0)
#define HAL_IO_PUSHPULL(port, idx)  do{(port)->OTYPER &= ~(1UL<<(idx));}while(0)


//Siwastaja macros
//uses unshifted pin numbers, not compatible with HAL defines
// Outputs:
#define HI(port, idx) do{(port)->BSRR = 1UL<<(idx);}while(0)
#define LO(port, idx) do{(port)->BSRR = 1UL<<(16+idx);}while(0)

// Inputs:
#define IN(port, idx) ((port)->IDR & (1UL<<(idx)))
// Always 0 or 1:
#define IN_LOGICAL(port, idx) (!!((port)->IDR & (1UL<<(idx))))

// Configuration:
#define IO_TO_GPI(port, idx) do{ uint32_t _tmp_ = (port)->MODER; _tmp_ &= ~(0b11UL<<((idx)*2)); (port)->MODER = _tmp_; }while(0)
#define IO_TO_GPO(port, idx) do{ uint32_t _tmp_ = (port)->MODER; _tmp_ &= ~(1UL<<((idx)*2+1)); _tmp_ |= 1UL<<((idx)*2); (port)->MODER = _tmp_; }while(0)
#define IO_TO_ALTFUNC(port, idx) do{ uint32_t _tmp_ = (port)->MODER; _tmp_ &= ~(1UL<<((idx)*2)); _tmp_ |= 1UL<<((idx)*2+1); (port)->MODER = _tmp_; }while(0)
#define IO_TO_ANALOG(port, idx) do{ uint32_t _tmp_ = (port)->MODER; _tmp_ |= 0b11<<((idx)*2); (port)->MODER = _tmp_; }while(0)
#define IO_PULLUP_ON(port, idx) do{ uint32_t _tmp_ = (port)->PUPDR; _tmp_ &= ~(1UL<<((idx)*2+1)); _tmp_ |= 1UL<<((idx)*2); (port)->PUPDR = _tmp_; }while(0)
#define IO_PULLDOWN_ON(port, idx) do{ uint32_t _tmp_ = (port)->PUPDR; _tmp_ &= ~(1UL<<((idx)*2)); _tmp_ |= 1UL<<((idx)*2+1); (port)->PUPDR = _tmp_; }while(0)
#define IO_PULLUPDOWN_OFF(port, idx) do{ uint32_t _tmp_ = (port)->PUPDR; _tmp_ &= ~(0b11UL<<((idx)*2)); (port)->PUPDR = _tmp_; }while(0)

#define IO_PUSHPULL(port, idx)  do{(port)->OTYPER &= ~(1UL<<(idx));}while(0)
#define IO_OPENDRAIN(port, idx) do{(port)->OTYPER |=  (1UL<<(idx));}while(0)

#define IO_SPEED(port, idx, speed) do{uint32_t _tmp_ = (port)->OSPEEDR; _tmp_ &= ~(0b11UL<<((idx)*2)); _tmp_ |= (speed)<<((idx)*2); (port)->OSPEEDR = _tmp_;  }while(0)

#define IO_SET_ALTFUNC(port, pin, af) do{ _Pragma("GCC diagnostic push") _Pragma("GCC diagnostic ignored \"-Wshift-count-negative\"")  _Pragma("GCC diagnostic ignored \"-Wshift-count-overflow\"") \
	if((pin)<8) {uint32_t _tmp_ = (port)->AFR[0]; _tmp_ &= ~(0b1111UL<<((pin)*4));     _tmp_ |= af<<((pin)*4);     (port)->AFR[0] = _tmp_;} \
        else {uint32_t _tmp_ = (port)->AFR[1]; _tmp_ &= ~(0b1111UL<<(((pin)-8)*4)); _tmp_ |= af<<(((pin)-8)*4); (port)->AFR[1] = _tmp_;} _Pragma("GCC diagnostic pop") }while(0)

#define IO_ALTFUNC(port, pin, af) do{ IO_TO_ALTFUNC((port),(pin)); IO_SET_ALTFUNC((port),(pin),(af));}while(0)

//Debug cycle counter, available on F1+
/*
void DWT_Init(void)
{
	if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk))
		CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

	DWT->CYCCNT = 0;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}

DWT->CYCCNT
	*/