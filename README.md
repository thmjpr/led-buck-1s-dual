# LED Buck module HV

![image](xx . jpg)



## Synopsis

Board to convert 1S cell voltage (~4V) to run two LED lights <4V (white, red, blue, etc.)

Two current regulated outputs, adjustable.
One low side mosfet switches.
Battery voltage monitoring.


## Hardware

- STM32F030F4P6 microcontroller
- PAM2804 LED driver (6-30V 1.2A) x 2
- SOT-23 N-FET x 1 
- 4.7uH inductor x 2


## PCB
- Rev_1: 
- Working?

- Rev_2:


## Issues
- 


## License

MIT license where applicable.